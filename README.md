Catholic Content Management System (CCMS)
Description

A custom CMS that allows the management of liturgical content for the Catholic church.

Features

CCMS leverages romcal to retrieve the liturgical calendar for a given year.

User profiles

User profiles built
Configurable notifications for
Upcoming Solemnities
Upcoming Memorials/Optional Memorials
Upcoming Feasts & Feasts of the Lord
Preloaded MongoDB Database containing

Daily readings for each day in the liturgical year (Cycles A, B, C)
Links and resources to the Saint of the day
Revisions

0.0.1

Documentation

TBC

Installation

TBC

Info

a Sails application